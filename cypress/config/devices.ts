export const returnDevice = (name: string) =>
  allDevices.filter((e) => e.name == name);

const allDevices = [
  {
    name: "Note10",
    userAgent:
      "Mozilla/5.0 (Linux; Android 10; SM-N970F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Mobile Safari/537.36",
    viewportWidth: 412,
    viewportHeigh: 868,
  },
  {
    name: "iPhoneX",
    userAgent:
      "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1",
    viewportWidth: 375,
    viewportHeigh: 812,
  },
];
