import { Locale } from "./locales";

export const returnBaseUrl = (countryCode: Locale, env) => {
  const country = allEnvs.filter((e) => e.code == countryCode);
  return country[0][env];
};

const allEnvs = [
  {
    code: Locale.de_DE,
    country: "Germany",
    production: "https://m.aboutyou.de",
  },
];
