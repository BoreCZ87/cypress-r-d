import { returnBaseUrl } from "../config/baseUrls";
import { Locale } from "../config/locales";

describe("page", () => {
  it("works", () => {
    // cy.visit("https://google.it");
    cy.visit("/");
    cy.url().then((url) => console.log(url));
    cy.log(Cypress.env("code"));
    cy.log(returnBaseUrl(Locale.de_DE, "production"));
  });
});
