// @ts-nocheck
// const puppeteer = require('puppeteer')
// const puppeteerConnect = require('../integration/R&D/puppeteerConnect')
import { Locale } from '../config/locale'
import { returnDevice } from '../config/devices'
import { returnBaseUrl as url } from '../config/baseUrls'

let debuggingPort
const screenshots = []

module.exports = (on, config) => {
  require('cypress-plugin-retries/lib/plugin')(on)

  const configOverride = {}
  if (config.env.userAgent) {
    config.env.userAgent = config.env.userAgent ? config.env.userAgent : 'Note10'
    const device = returnDevice(config.env.userAgent)
    configOverride.userAgent = device[0].userAgent
    configOverride.viewportWidth = device[0].viewportWidth
    configOverride.viewportHeigh = device[0].viewportHeigh
    config.env['deviceName'] = device[0].name
  } else {
    configOverride.userAgent = 'none'
  }

  config.env.environment = config.env.environment ? config.env.environment : 'production'
  config.env.code = process.env.CODE ? process.env.CODE : Locale.de_DE
  config.baseUrl = url(config.env.code, config.env.environment)

  on('before:browser:launch', (browser = {}, launchOptions) => {
    if (browser.family === 'chromium' && browser.name !== 'electron') {
      if (browser.name === 'chrome') {
        launchOptions.args.push('--disable-dev-shm-usage')
      }
      // auto open devtools
      launchOptions.args.push('--auto-open-devtools-for-tabs')
      const existing = launchOptions.args.find(
        arg => arg.slice(0, 23) === '--remote-debugging-port',
      )
      debuggingPort = existing.split('=')[1]
    }
    if (browser.family === 'firefox') {
      // auto open devtools
      launchOptions.args.push('-devtools')
    }
    return launchOptions
  })

  on('task', {
    // async puppeteerConnect() {
    //   return await puppeteerConnect(debuggingPort)
    // },
    screenshots() {
      return screenshots
    },
  })

  // return configuration
  return Object.assign({}, config, configOverride)
}
